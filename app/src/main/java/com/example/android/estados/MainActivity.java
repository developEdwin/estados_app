package com.example.android.estados;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.view.View.OnClickListener;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        LinearLayout gasideal = (LinearLayout) findViewById(R.id.gas_ideal_btn);

        gasideal.setOnClickListener(new OnClickListener() {
            // The code in this method will be executed when the gas ideal button is clicked on.
            @Override
            public void onClick(View view) {
                // Create a new intent to open the {@link GasIdealActivity}
                Intent gasidealIntent = new Intent(MainActivity.this, GasIdealActivity.class);
                // Start the new activity
                startActivity(gasidealIntent);
            }
        });

        LinearLayout dieterici = (LinearLayout) findViewById(R.id.dieterici_btn);

        dieterici.setOnClickListener(new OnClickListener() {
            // The code in this method will be executed when the dieterici button is clicked on.
            @Override
            public void onClick(View view) {
                // Create a new intent to open the {@link DietericiActivity}
                Intent dietericiIntent = new Intent(MainActivity.this, DietericiActivity.class);
                // Start the new activity
                startActivity(dietericiIntent);
            }
        });

        LinearLayout vanderwaals = (LinearLayout) findViewById(R.id.vanderwaals_btn);

        vanderwaals.setOnClickListener(new OnClickListener() {
            // The code in this method will be executed when the vanderwaals button is clicked on.
            @Override
            public void onClick(View view) {
                // Create a new intent to open the {@link VanDerWaalsActivity}
                Intent vanderwaalsIntent = new Intent(MainActivity.this, VanDerWaalsActivity.class);
                // Start the new activity
                startActivity(vanderwaalsIntent);
            }
        });

        LinearLayout margules = (LinearLayout) findViewById(R.id.marg_btn);

        margules.setOnClickListener(new OnClickListener() {
            // The code in this method will be executed when the vanderwaals button is clicked on.
            @Override
            public void onClick(View view) {
                // Create a new intent to open the {@link MargulesActivity}
                Intent margulesIntent = new Intent(MainActivity.this, MargulesActivity.class);
                // Start the new activity
                startActivity(margulesIntent);
            }
        });

    }
}
