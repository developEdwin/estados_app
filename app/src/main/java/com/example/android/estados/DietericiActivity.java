package com.example.android.estados;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Locale;

import org.apache.commons.math3.analysis.UnivariateFunction;
import org.apache.commons.math3.analysis.solvers.UnivariateSolver;
import org.apache.commons.math3.analysis.solvers.BrentSolver;

import org.apache.commons.math3.util.FastMath;

public class DietericiActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dieterici);

        TextView presionBtn = (TextView) findViewById(R.id.calc_P_btn);
        presionBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                calcularPresion(view);
            }
        });

        TextView volumenBtn = (TextView) findViewById(R.id.calc_V_btn);
        volumenBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                calcularVolumen(view);
            }
        });

        TextView tempBtn = (TextView) findViewById(R.id.calc_T_btn);
        tempBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                calcularTemperatura(view);
            }
        });

        TextView resetButton = (TextView) findViewById(R.id.reset_btn);

        resetButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                borrarTodo(view);
            }
        });

        //Código para listas de unidades
        //Volumen
        Spinner volumenSpinner = (Spinner) findViewById(R.id.vol_spinner);
        ArrayAdapter<CharSequence> volumenAdapter = ArrayAdapter.createFromResource(this,
                R.array.volum_array, android.R.layout.simple_spinner_item);
        volumenAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        volumenSpinner.setAdapter(volumenAdapter);
        volumenSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int pos, long id) {
                elegirRV(pos);
                elegirUniV(pos);
            }

            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        //Presión
        Spinner presionSpinner = (Spinner) findViewById(R.id.presion_spinner);
        ArrayAdapter<CharSequence> presionAdapter = ArrayAdapter.createFromResource(this,
                R.array.presion_array, android.R.layout.simple_spinner_item);
        presionAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        presionSpinner.setAdapter(presionAdapter);
        presionSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int pos, long id) {
                elegirRP(pos);
                elegirUniP(pos);
            }

            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        //Parámetro a
        Spinner paramASpinner = (Spinner) findViewById(R.id.param_a_spinner);
        ArrayAdapter<CharSequence> paramAAdapter = ArrayAdapter.createFromResource(this,
                R.array.param_a_array, android.R.layout.simple_spinner_item);
        paramAAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        paramASpinner.setAdapter(paramAAdapter);
        paramASpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int pos, long id) {
                elegirParamA(pos);
                elegirUniParamA(pos);
            }

            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        //Parámetro b
        Spinner paramBSpinner = (Spinner) findViewById(R.id.param_b_spinner);
        ArrayAdapter<CharSequence> paramBAdapter = ArrayAdapter.createFromResource(this,
                R.array.param_b_array, android.R.layout.simple_spinner_item);
        paramBAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        paramBSpinner.setAdapter(paramBAdapter);
        paramBSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int pos, long id) {
                elegirParamB(pos);
                elegirUniParamB(pos);
            }

            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        //Temperatura
        Spinner tempSpinner = (Spinner) findViewById(R.id.temp_spinner);
        ArrayAdapter<CharSequence> tempAdapter = ArrayAdapter.createFromResource(this,
                R.array.temp_array, android.R.layout.simple_spinner_item);
        tempAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        tempSpinner.setAdapter(tempAdapter);
        tempSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int pos, long id) {
                elegirUniT(pos);
            }

            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }

    double volum, par_a, par_b, temp, presion, moles;
    double R_p, R_v, cte_R, R_par_a, R_par_b;
    final double R_1 = 0.082057338; // atm*lts/mol*K
    String uni_P, uni_V, uni_T, uni_par_a, uni_par_b;

    public void elegirRP(int pos) { // Cambio en la constante R por unidades de presión
        switch (pos) {
            case 0:
                R_p = 1; // atm
                break;
            case 1:
                R_p = 101325; // Pa
                break;
            case 2:
                R_p = 1.01325; // bar
                break;
            case 3:
                R_p = 760.002; // mmHg
                break;
            case 4:
                R_p = 14.6959; // PSI
                break;
        }
    }

    public void elegirUniP(int pos) { // Cambio en la presentación del resultado por unidades de presión
        switch (pos) {
            case 0:
                uni_P = "atm";
                break;
            case 1:
                uni_P = "Pa";
                break;
            case 2:
                uni_P = "bar";
                break;
            case 3:
                uni_P = "mmHg";
                break;
            case 4:
                uni_P = "PSI";
                break;
        }
    }

    public void elegirRV(int pos) { // Cambio en la constante R por unidades de volumen
        switch (pos) {
            case 0:
                R_v = 1; // L
                break;
            case 1:
                R_v = 1e-3; // m^3
                break;
        }
    }

    public void elegirUniV(int pos) { // Cambio en la presentación del resultado por unidades de volumen
        switch (pos) {
            case 0:
                uni_V = "L";
                break;
            case 1:
                uni_V = "m^3";
                break;
        }
    }

    public void elegirParamA(int pos) {
        switch (pos) {
            case 0:
                R_par_a = 1;//atm*L^2/mol^2
                break;
            case 1:
                R_par_a = 1.0/0.101325;//J*m^3/mol^2
                break;

        }
    }

    public void elegirUniParamA(int pos) {
        switch (pos) {
            case 0:
                uni_par_a = "atm-L^2/mol^2";
                break;
            case 1:
                uni_par_a = "J-m^3/mol^2";
                break;
        }
    }

    public void elegirParamB(int pos) {
        switch (pos) {
            case 0:
                R_par_b = 1;// L/mol
                break;
            case 1:
                R_par_b = 1e-3;// m^3/mol
                break;
        }
    }

    public void elegirUniParamB(int pos) {
        switch (pos) {
            case 0:
                uni_par_b = "L/mol";
                break;
            case 1:
                uni_par_b = "m^3/mol";
        }
    }

    public double convertirTemp(int pos, double T) {
        switch (pos) {
            case 0: // Temperatura dada en K
                break;
            case 1: // Temperatura dada en °C
                T += 272.15;
                break;
            case 2: // Temperatura dada en °F
                T = 5*(T-32)/9 + 272.15;
                break;
            case 3: // Temperatura dada en R
                T *= 5/9;
                break;
        }

        return T;
    }

    public double convertirTempenTemp(int pos, double T) {
        switch (pos) {
            case 0: // Temperatura dada en K
                break;
            case 1: // Temperatura dada en °C
                T -= 272.15;
                break;
            case 2: // Temperatura dada en °F
                T = 5*(T-32)/9 + 272.15;
                break;
            case 3: // Temperatura dada en R
                T *= 5/9;
                break;
        }

        return T;
    }

    public void elegirUniT(int pos) { // Cambio en la presentación del resultado por unidades de temperatura
        switch (pos) {
            case 0:
                uni_T = "K";
                break;
            case 1:
                uni_T = "°C";
                break;
            case 2:
                uni_T = "°F";
                break;
            case 3:
                uni_T = "R";
                break;
        }
    }

    public void calcularPresion(View view) {
        EditText volumEditText = (EditText) findViewById(R.id.volum_edit_text);
        EditText parAEditText = (EditText) findViewById(R.id.par_a_edit_text);
        EditText parBEditText = (EditText) findViewById(R.id.par_b_edit_text);
        EditText tempEditText = (EditText) findViewById(R.id.temp_edit_text);
        EditText molesEditText = (EditText) findViewById(R.id.moles_edit_text);

        // Revisar si los campos están vacíos
        if(TextUtils.isEmpty(volumEditText.getText().toString())) {
            Toast.makeText(this, R.string.falta_algo, Toast.LENGTH_SHORT).show();
            return;
        }
        else {
            volum = Double.valueOf(volumEditText.getText().toString())*R_v;
        }

        if(TextUtils.isEmpty(parAEditText.getText().toString())) {
            Toast.makeText(this, R.string.falta_algo, Toast.LENGTH_SHORT).show();
            return;
        }
        else {
            par_a = Double.valueOf(parAEditText.getText().toString())*R_par_a;
        }

        if(TextUtils.isEmpty(parBEditText.getText().toString())) {
            Toast.makeText(this, R.string.falta_algo, Toast.LENGTH_SHORT).show();
            return;
        }
        else {
            par_b = Double.valueOf(parBEditText.getText().toString())*R_par_b;
        }

        if(TextUtils.isEmpty(molesEditText.getText().toString())) {
            Toast.makeText(this, R.string.falta_algo, Toast.LENGTH_SHORT).show();
            return;
        }
        else {
            moles = Double.valueOf(molesEditText.getText().toString());
        }

        if(TextUtils.isEmpty(tempEditText.getText().toString())) {
            Toast.makeText(this, R.string.falta_algo, Toast.LENGTH_SHORT).show();
            return;
        }
        else {
            temp = Double.valueOf(tempEditText.getText().toString());
            Spinner tempSpinner = (Spinner) findViewById(R.id.temp_spinner);
            int pos = tempSpinner.getSelectedItemPosition();
            temp = convertirTemp(pos, temp);
        }

        presion = moles*R_1*temp*(FastMath.exp(-moles*par_a/(volum*R_1*temp)))/
                (volum-(par_b*moles));
        String result = "P = " + String.format(Locale.ENGLISH,"%.4f", presion*R_p) + " " + uni_P;

        mostrarResultado(result);
    }

    public void calcularVolumen(View view) {
        EditText presionEditText = (EditText) findViewById(R.id.presion_edit_text);
        EditText parAEditText = (EditText) findViewById(R.id.par_a_edit_text);
        EditText parBEditText = (EditText) findViewById(R.id.par_b_edit_text);
        EditText tempEditText = (EditText) findViewById(R.id.temp_edit_text);
        EditText molesEditText = (EditText) findViewById(R.id.moles_edit_text);

        // Revisar si los campos están vacíos
        if(TextUtils.isEmpty(presionEditText.getText().toString())) {
            Toast.makeText(this, R.string.falta_algo, Toast.LENGTH_SHORT).show();
            return;
        }
        else {
            presion = Double.valueOf(presionEditText.getText().toString())*R_p;
        }

        if(TextUtils.isEmpty(parAEditText.getText().toString())) {
            Toast.makeText(this, R.string.falta_algo, Toast.LENGTH_SHORT).show();
            return;
        }
        else {
            par_a = Double.valueOf(parAEditText.getText().toString())*R_par_a;
        }

        if(TextUtils.isEmpty(parBEditText.getText().toString())) {
            Toast.makeText(this, R.string.falta_algo, Toast.LENGTH_SHORT).show();
            return;
        }
        else {
            par_b = Double.valueOf(parBEditText.getText().toString())*R_par_b;
        }

        if (TextUtils.isEmpty(molesEditText.getText().toString())) {
            Toast.makeText(this, R.string.falta_algo, Toast.LENGTH_SHORT).show();
            return;
        }
        else {
            moles = Double.valueOf(molesEditText.getText().toString());
        }

        if(TextUtils.isEmpty(tempEditText.getText().toString())) {
            Toast.makeText(this, R.string.falta_algo, Toast.LENGTH_SHORT).show();
            return;
        }
        else {
            temp = Double.valueOf(tempEditText.getText().toString());
            Spinner tempSpinner = (Spinner) findViewById(R.id.temp_spinner);
            int pos = tempSpinner.getSelectedItemPosition();
            temp = convertirTemp(pos, temp);
        }

        UnivariateFunction f_volumen = new UnivariateFunction() {
            @Override
            public double value(double v) {
                return (presion*(v-(moles*par_b))*
                        (FastMath.exp(moles*par_a/(R_1*temp*v))))-(moles*R_1*temp);
            }
        };
        UnivariateSolver solver = new BrentSolver();
        volum = solver.solve(100, f_volumen, 0, 10000);
        String vConUni = "V = " + String.format(Locale.ENGLISH,"%.4f", volum*R_v) + " " + uni_V;

        mostrarResultado(vConUni);
    }

    public void calcularTemperatura(View view) {
        EditText presionEditText = (EditText) findViewById(R.id.presion_edit_text);
        EditText parAEditText = (EditText) findViewById(R.id.par_a_edit_text);
        EditText parBEditText = (EditText) findViewById(R.id.par_b_edit_text);
        EditText volumenEditText = (EditText) findViewById(R.id.volum_edit_text);
        EditText molesEditText = (EditText) findViewById(R.id.moles_edit_text);

        // Revisar si los campos están vacíos
        if(TextUtils.isEmpty(presionEditText.getText().toString())) {
            Toast.makeText(this, R.string.falta_algo, Toast.LENGTH_SHORT).show();
            return;
        }
        else {
            presion = Double.valueOf(presionEditText.getText().toString())*R_p;
        }

        if(TextUtils.isEmpty(parAEditText.getText().toString())) {
            Toast.makeText(this, R.string.falta_algo, Toast.LENGTH_SHORT).show();
            return;
        }
        else {
            par_a = Double.valueOf(parAEditText.getText().toString())*R_par_a;
        }

        if(TextUtils.isEmpty(parBEditText.getText().toString())) {
            Toast.makeText(this, R.string.falta_algo, Toast.LENGTH_SHORT).show();
            return;
        }
        else {
            par_b = Double.valueOf(parBEditText.getText().toString())*R_par_b;
        }

        if (TextUtils.isEmpty(molesEditText.getText().toString())) {
            Toast.makeText(this, R.string.falta_algo, Toast.LENGTH_SHORT).show();
            return;
        }
        else {
            moles = Double.valueOf(molesEditText.getText().toString());
        }

        if(TextUtils.isEmpty(volumenEditText.getText().toString())) {
            Toast.makeText(this, R.string.falta_algo, Toast.LENGTH_SHORT).show();
            return;
        }
        else {
            volum = Double.valueOf(volumenEditText.getText().toString())*R_v;
        }

        UnivariateFunction f_temp = new UnivariateFunction() {
            @Override
            public double value(double v) {
                return (presion*(volum-(moles*par_b))*
                        (FastMath.exp(moles*par_a/(R_1*v*volum))))-(moles*R_1*v);
            }
        };
        UnivariateSolver solver = new BrentSolver();
        temp = solver.solve(100, f_temp, 0, 10000);
        Spinner tempSpinner = (Spinner) findViewById(R.id.temp_spinner);
        int pos = tempSpinner.getSelectedItemPosition();
        temp = convertirTempenTemp(pos, temp);
        String tConUni = "V = " + String.format(Locale.ENGLISH,"%.4f", temp) + " " + uni_T;

        mostrarResultado(tConUni);
    }

    private void mostrarResultado(String message) {
        TextView resultadoTextView = (TextView) findViewById(R.id.resultado_text_view);
        resultadoTextView.setText(message);
    }

    public void borrarTodo(View view) {
        EditText volumEditText = (EditText) findViewById(R.id.volum_edit_text);
        EditText presEditText = (EditText) findViewById(R.id.presion_edit_text);
        EditText parAEditText = (EditText) findViewById(R.id.par_a_edit_text);
        EditText parBEditText = (EditText) findViewById(R.id.par_b_edit_text);
        EditText tempEditText = (EditText) findViewById(R.id.temp_edit_text);
        EditText molesEditText = (EditText) findViewById(R.id.moles_edit_text);

        volumEditText.setText(null);
        presEditText.setText(null);
        parAEditText.setText(null);
        parBEditText.setText(null);
        tempEditText.setText(null);
        molesEditText.setText(null);
        mostrarResultado("0");
    }
}
